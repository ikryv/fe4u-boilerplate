import { randomUserMock, additionalUsers } from './FE4U-Lab2-mock.js';
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();


/** ******** Your code here! *********** */

function randomCourse() {
  const courses = ['Mathematics', 'Physics', 'English', 'Computer Science', 'Dancing', 'Chess', 'Biology', 'Chemistry','Law', 'Art', 'Medicine', 'Statistics'];
  return courses[Math.floor(Math.random() * courses.length)];
}

function randomId() {
  const letters = 'abcdefghijklmnopqrstuvwxyz0123456789';
  let id = '';
  for (let i = 0; i < 32; i++) {
    id += letters[Math.floor(Math.random() * letters.length)];
  }
  return id;
}

function randomColor() {
  const letters = '0123456789abcdef';
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * letters.length)];
  }
  return color;
}

// task1
function getUsers(users) {
  const result = [];

  users.forEach(user => {
    const reformattedUser = {
      title: user.name.title,
      firstName: user.name.first,
      lastName: user.name.last,
      full_name: `${user.name.first} ${user.name.last}`,
      gender: user.gender,
      city: user.location.city,
      state: user.location.state,
      country: user.location.country,
      postcode:  user.location.postcode,
      coordinates:  user.location.coordinates,
      timezone: user.location.timezone,
      email: user.email,
      b_day: user.dob.date,
      age: user.dob.age,
      phone: user.phone,
      picture_large: user.picture.large,
      picture_thumbnail: user.picture.thumbnail,
      course: randomCourse(),
      bg_color: randomColor(),
      note: 'Note',
      nat: user.nat,
      favourite: (Math.random() < 0.1) ? true : false,
    };

    if (user.id.name && user.id.value) {
      reformattedUser.id = `${user.id.name}${user.id.value}`;
    } else {
      reformattedUser.id = randomId();
    }

    result.push(reformattedUser);
  });
  
  return result;
}

function getUniqueUsers(firstArray, secondArray) {
  const ids = new Set();
  secondArray.forEach(user => {
    const name = user.full_name.split(' ')
    user.firstName = name[0]
    user.lastName = name[1]
    if (!user.course) user.course = randomCourse()
  })
  const bothArrays = [...firstArray, ...secondArray];
  const result = [];
  bothArrays.forEach(user => {
    if (!ids.has(user.id)) {
      result.push(user);
      ids.add(user.id);
    }
  })
  return result;
}

//task 2
function validateUser(user) {
  const stringFields = ['full_name', 'note', 'course', 'city', 'country'];
  for (const field of stringFields) {
    if (typeof (user[field]) !== 'string' || !user[field] || user[field][0] !== user[field][0].toUpperCase()) {
      return { isValid: false, message: `Property ${field} should be defined and start with upper case`};
    }
  };
  if (!user.gender || user.gender[0] !== user.gender[0].toLowerCase()) return { isValid: false, message: `Gender should be defined and start with lower case`};

  if (typeof user.age !== 'number' || isNaN(user.age) || user.age <= 0 || user.age >= 150) return { isValid: false, message: `Age should be defined and be within range : (0, 150)`};
  if (!user.email || !user.email.includes('@')) return { isValid: false, message: `Email should be valid`};
  if (!user.phone || !user.nat) return { isValid: false, message: `Phone should be defined`};
  if (user.full_name.split(' ').length < 2) {
    return { isValid: false, message: `Name should have first and last names separated by space`}
  }

  const validPhone = phoneUtil.isValidNumberForRegion(phoneUtil.parse(user.phone, user.nat), user.nat);
  if (!validPhone) {
    return { isValid: false, message: `Phone should be valid for the country`};
  } else {
    return { isValid: true }
  }
}


const regionMap = {
  "United States": "America",
  "Germany": "Europe",
  "Ireland": "Europe",
  "Australia": "Oceania",
  "Finland": "Europe",
  "Turkey": "Asia",
  "Switzerland": "Europe",
  "New Zealand": "Oceania",
  "Spain":"Europe",
  "Norway":"Europe",
  "Denmark":"Europe",
  "Iran":"Asia",
  "Canada":"America",
  "France":"Europe",
  "Netherlands":"Europe",
  "Ukraine": "Europe",
}
//task 3
function filterUsers(users, params) {
  return users.filter(user => {
    if (params.gender && user.gender !== params.gender) {
      return false;
    }
    if (params.age) {
      const ageRange = params.age.split('-')
      const min = parseInt(ageRange[0])
      const max = parseInt(ageRange[1])
      if (min > user.age || max < user.age) {
        return false;
      }
    }
    if (params.region && regionMap[user.country] !== params.region) {
      return false;
    }
    if (typeof params.favourite === 'boolean' && user.favourite !== params.favourite) {
      return false;
    }
    if (params.photo && (!user.picture_large || !user.picture_thumbnail)) {
      return false;
    }
    return true;
  })
}

//task 4
function sortUsers(users, param, ascending) {
  const result = [...users]
  const ascendingModifier = ascending ? 1 : -1;

  result.sort((user1, user2) => {
    if (user1[param] === user2[param]) return 0;

    if (!user1[param]) return -1 * ascendingModifier;
    if (!user2[param]) return 1 * ascendingModifier;
    
    return user1[param] > user2[param] ? 1 * ascendingModifier : -1 * ascendingModifier; 
  });
  return result;
}

// task 5

function findUser(users, param, ageOption) {
  const foundUsers = findUsers(users, param, ageOption);
  return foundUsers.length ? foundUsers[0] : null;
}

function findUsers(users, param, ageOption) {
  if (param.age) {
    return users.filter(user => {
      if (ageOption === '<') return user.age < param.age;
      if (ageOption === '>') return user.age > param.age;
      return user.age === param.age;
    })
  }
  
  return users.filter(user => {
    if (param.note) {
      if (user.note && user.note.toLowerCase().includes(param.note.toLowerCase())) return true;
    }
    if (param.name) {
      return user.full_name && user.full_name.toLowerCase().includes(param.name.toLowerCase());
    }
    return false
  })
}

//task 6
function findPercentage(users, param, ageOption) {
  const foundUsers = findUsers(users, param, ageOption);
  const percentage = ((foundUsers.length / users.length) * 100).toFixed(2);
  return percentage;
}

/*const users = getUsers(randomUserMock);
const allUsers = getUniqueUsers(users, additionalUsers);
let favouriteUsers = filterUsers(allUsers, { favourite: true })
let favouritesRowIndex = 0
console.log(allUsers);

const user1 = allUsers[0];
console.log('is valid ' + validateUser(user1));

user1.full_name = "name"
console.log('is valid ' + validateUser(user1).message);
console.log([...new Set(allUsers.map(user => user.country))])

const params = {
  country: 'Norway',
  gender: 'male',
};
console.log(filterUsers(allUsers, params));

console.log(sortUsers(allUsers, 'age', false));


console.log(findUser(allUsers, { name: 'August'}));
console.log(findUser(allUsers, { age: 30 }, '>'));

console.log(findPercentage(allUsers, { age: 30 }, '>'));*/

let allUsers = []
let favouriteUsers = filterUsers(allUsers, { favourite: true })
let favouritesRowIndex = 0

function onTeacherThumbNailClick(teacher) {
  document.getElementById("teacher-modal-info__name").innerText = teacher.full_name
  document.getElementById("teacher-modal-info__speciality").innerText = teacher.course
  document.getElementById("teacher-modal-info__region").innerText = `${teacher.city}, ${teacher.country}`
  document.getElementById("teacher-modal-info__age").innerText = `${teacher.age}, ${teacher.gender}`
  document.getElementById("teacher-modal-info__mail").innerText = teacher.email
  document.getElementById("teacher-modal-info__number").innerText = teacher.phone
  document.getElementById("teacher-modal-info__notes").innerText = teacher.note
  document.getElementById("teacher-modal-info__image").src = teacher.picture_large
  const el = document.getElementById("teacher-modal-info__star")
  const elClone = el.cloneNode(true);

  el.parentNode.replaceChild(elClone, el);
  elClone.addEventListener('click', () => {
    teacher.favourite = !teacher.favourite
    favouriteUsers = filterUsers(allUsers, { favourite: true })
    setFavourites()
  })
}

function setFavourites() {
  const grid = document.getElementById("favourites-grid")
  grid.innerHTML = `<button class="arrow-btn" id="arrow-btn-left"><img id="arrow-left" src="./images/arrow.jpg">`
  let teachers = []

  for (let i = favouritesRowIndex; i < favouritesRowIndex + 4 && i < favouriteUsers.length; i++) {
    teachers.push(favouriteUsers[i])
  }
  teachers.forEach(teacher => {
    grid.innerHTML += 
      `</button><a class="card-link" href="#card-popup"  id="${teacher.id}-favourite">
        <figure class="teacher-item">
            <div class="teacher-item__profile-border" style="border-color: ${teacher.bg_color};">
                <div class="teacher-item__profile-img" style="background-image: url(${teacher.picture_thumbnail});"></div>
            </div>
            <figcaption class="teacher-item__info">
                <span class="teacher-item__name">${teacher.firstName}</span><br>
                <span class="teacher-item__surname">${teacher.lastName}</span><br>
                <span class="teacher-item__country">${teacher.country}</span>
            </figcaption>
        </figure>
      </a>`
  });
  grid.innerHTML += `<button class="arrow-btn"  id="arrow-btn-right"><img id="arrow-right" src="./images/arrow.jpg"></button>`

  teachers.forEach(teacher => {
    document.getElementById(`${teacher.id}-favourite`).addEventListener('click', () => onTeacherThumbNailClick(teacher))
  })
  document.getElementById("arrow-btn-left").addEventListener("click", () => {
    if (favouritesRowIndex > 0) {
      favouritesRowIndex--
      setFavourites()
    }
  })
  document.getElementById("arrow-btn-right").addEventListener("click", () => {
    if (favouritesRowIndex + 4 < favouriteUsers.length) {
      favouritesRowIndex++
      setFavourites()
    }
  })
}

function setTeachersGridHtml(teachers) {
  const grid = document.getElementById("teacher-grid")
  grid.innerHTML = ""
  teachers.forEach(teacher => {
    grid.innerHTML += 
      `<a class="card-link" href="#card-popup"  id="${teacher.id}-grid">
        <figure class="teacher-item">
            <div class="teacher-item__profile-border" style="border-color: ${teacher.bg_color};">
                <div class="teacher-item__profile-img" style="background-image: url(${teacher.picture_thumbnail});"></div>
            </div>
            <figcaption class="teacher-item__info">
                <span class="teacher-item__name">${teacher.firstName}</span><br>
                <span class="teacher-item__surname">${teacher.lastName}</span><br>
                <span class="teacher-item__specialty">${teacher.course}</span><br>
                <span class="teacher-item__country">${teacher.country}</span>
            </figcaption>
        </figure>
      </a>`
    
  });

  teachers.forEach(teacher => {
    document.getElementById(`${teacher.id}-grid`).addEventListener('click', () => onTeacherThumbNailClick(teacher))
  })
}

const filterParams = {}
let findParams = null
let sortParam = null
let isAscendind = true
let page = 1
let seed = null
let fetchPage = 1

function onFilterChange() {
  let filtered = filterUsers(allUsers, filterParams)
  if (findParams) {
    filtered = findUsers(filtered, findParams)
  }
  page = 1
  setStatisticsHtml(filtered)
  setTeachersGridHtml(filtered)

}

function setStatisticsHtml(users) {
  const sortedTeachers = sortParam ? sortUsers(users, sortParam, isAscendind) : [...users]
  const teachers = sortedTeachers.splice((page - 1) * 10, 10)
  console.log((page - 1) * 10)
  console.log( page * 10)

  const statistics = document.getElementById("statistics")
  statistics.innerHTML = `<tr id="statistics-header">
    <th title="full_name">Name</th>
    <th title="course">Specialty</th>
    <th title="age">Age</th>
    <th title="gender">Gender</th>
    <th title="country">Nationality</th>
  </tr>`
  teachers.forEach(teacher => {
    statistics.innerHTML += 
      `<tr>
        <td>${teacher.full_name}</th>
        <td>${teacher.course}</th>
        <td>${teacher.age}</th>
        <td>${teacher.gender}</th>
        <td>${teacher.country}</th>
      </tr>`
  })

  const pagination = document.getElementById("statistics-pagination")
  let pageArray = [page - 1, page, page + 1]
  if (page === 1) pageArray = [1, 2, 3]
  if (page * 10 >= users.length) pageArray = [page - 2, page - 1, page]
   pageArray = pageArray.filter(page => page > 0 && (page - 1) * 10 < users.length)
  pagination.innerHTML = ''
  pageArray.forEach(p => {
    pagination.innerHTML += `<span id="${p}-page">${p}</span>`
  }) 
  pagination.innerHTML += `<span>...</span>
  <span class="statistics-pagination-last" id="page-last">Last</span>`

  pageArray.forEach(p => {
    const el = document.getElementById(`${p}-page`)
    el.addEventListener('click', () => {
      page = p
      setStatisticsHtml(users)
    })
  })
  const last = document.getElementById("page-last")
  last.addEventListener('click', () => {
    page = Math.floor((users.length - 1) / 10) + 1
    setStatisticsHtml(users)
  })


  for (const child of document.getElementById("statistics-header").children) {
    if (child.title === sortParam) {
      if (isAscendind) {
        child.classList.add("statistics__active-sort-asc");
      } else {
        child.classList.add("statistics__active-sort-desc");
      }
    }
  }
  document.getElementById("statistics-header").childNodes.forEach(node => {
    node.addEventListener('click', (e) => {
      const param = e.target.title
      if (param === sortParam && isAscendind) {
        isAscendind = false
      } 
      else if (param === sortParam) {
        sortParam = null
        isAscendind = true
      }
      else {
        sortParam = param
        isAscendind = true
      }
      console.log(param)
      page = 1
      setStatisticsHtml(users)
    })
  })
}


const natMap = {
  "USA": 'US',
  "UK": "GB",
  "Ukraine": "UA",
  "France": "FR",
  "Turkey": "TR",
  "Germany": "DE",
}
function setAddTeacherListeners() {
  document.getElementById("add-teacher-close").addEventListener('click', (e) => {
    history.pushState({}, null, "http://localhost:3001/");
    document.getElementById("add-teacher-popup").style.display = 'none'
  })
  document.getElementById("add-btn").addEventListener('click', async (e) => {
    e.preventDefault()
    const user = {
      full_name: document.getElementById("name").value,
      course: document.getElementById("speciality").value,
      country: document.getElementById("country").value,
      city: document.getElementById("city").value,
      note: document.getElementById("notes").value,
      phone: document.getElementById("phone-number").value,
      email: document.getElementById("email").value,
      b_day: document.getElementById("date-of-birth").value,
      bg_color: document.getElementById("background-color").value,
      gender: 'male',
      id: randomId(),
    }

    if (document.getElementById("female-sex").checked) user.gender = 'female'
    user.nat = natMap[user.country]
    user.age = new Date().getFullYear() - new Date(user.b_day).getFullYear()
    const name = user.full_name.split(' ')
    user.firstName = name[0]
    if (name.length > 1) {
      user.lastName = name[1]
    }

    const validate = validateUser(user)
    if (!validate.isValid) {
      alert(validate.message)
      return
    }

    const res = await fetch('http://localhost:3000/teachers', {
      method: 'POST',
      body: JSON.stringify(user),
      headers: {
        "Content-Type": "application/json",
      }
    })
    console.log(res)
    if (!res.ok) return

    allUsers.push(user)
    onFilterChange()
    document.getElementById("add-teacher-popup").style.display = 'none'
  })
}

async function fetchUsers(count) {
  let url = `https://randomuser.me/api/?results=${count}`
  if (seed) {
    console.log(seed)
    fetchPage++
    url = `${url}&&seed=${seed}&&page=${fetchPage}`
  }
  const res = await fetch(url)
  const users = await res.json()

  if (!seed) {
    seed = users.info.seed
  }
  const formatted = getUsers(users.results)
  return formatted
}

async function fetchStoredUsers() {
  let url = `http://localhost:3000/teachers`
  const res = await fetch(url)
  const users = await res.json()
  console.log(users)
  return users
}

window.onload = async () => {
  allUsers = await fetchUsers(50)
  const dbUsers =  await fetchStoredUsers()
  allUsers = getUniqueUsers(allUsers, dbUsers)
  favouriteUsers = filterUsers(allUsers, { favourite: true })
  
  setTeachersGridHtml(allUsers)
  setFavourites()

  document.getElementById("favourites-checkbox").addEventListener('click', (e) => {
    filterParams.favourite = !filterParams.favourite
    onFilterChange()
  })

  document.getElementById("sex").addEventListener('click', (e) => {
    if (e.target.value === 'default') {
      filterParams.gender = null
    } else {
      filterParams.gender = e.target.value
    }
    onFilterChange()
  })

  document.getElementById("photo-checkbox").addEventListener('click', () => {
    filterParams.photo = !filterParams.photo
    onFilterChange()
  })

  document.getElementById("region-select").addEventListener('click', (e) => {
    if (e.target.value === 'default') {
      filterParams.region = null
    } else {
      filterParams.region = e.target.value
    }
    onFilterChange()
  })

  document.getElementById("age-select").addEventListener('click', (e) => {
    if (e.target.value === 'default') {
      filterParams.age = null
    } else {
      filterParams.age = e.target.value
    }
    onFilterChange()
  })

  document.getElementById("search-btn").addEventListener('click', (e) => {
    const value = document.getElementById("search-field").value
    if (!value) {
      findParams = null
      onFilterChange()
      return
    }
    if (isNaN(parseInt(value, 10))) {
      findParams = { name: value, note: value }
    } else {
      findParams = { age: parseInt(value, 10) }
    }
    console.log(findParams)
    onFilterChange()
  })

  setStatisticsHtml(allUsers)
  setAddTeacherListeners()
  document.getElementById("add-btn-top").addEventListener('click', () => {
    document.getElementById("add-teacher-popup").style.display = 'block'
  })

  document.getElementById("next-btn").addEventListener('click', async () => {
    const newUsers = await fetchUsers(10)
    allUsers.push(...newUsers)
    onFilterChange()
  })
}
